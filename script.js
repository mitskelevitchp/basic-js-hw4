// Теоретичні питання
// 1. Функції дозволяють писати легший для розуміння та більш функціональний код. Вони замінюють великі "шматки" коду, що повторюються (наприклад, цикли). Функція - це ніби шаблон операції чи кількох операцій, які можна застосовувати неодноразово, підставляючи до параметрів різні аргументи.
// 2. Аргумент функції можна отримати як змінну, прописану в коді як константа. Або ж зберегти як змінну, отримавши значення від користувача. Наприклад, функція може зберігати в собі операцію додавання 2 аргументів, які отримують своє значення вже після початку роботи коду.
// В залежності від аргументу/-тів, функція отримує різний результат. Це значно розширює можливості функцій, хоча не робить неможливим вирішення окремих задач без використання аргументів та зазначення параметрів в дужках.
// 3. return дозволяє повернути результат розрахунків, що відбуваються в тілі функції. Наприклад, коли потрібно зберегти підсумок розрахунків в окрему змінну з наступним її використанням в коді (поза функцією).
// Після return решта коду всередині функції не виконується. Це зручно, коли після виконання необхідних умов продовження подальших розрахунках не має сенсу.

// Практичне завдання
// Основне завдання. Калькулятор

// let numberFirst = +prompt("Give me your first number:");
// let numberSecond = +prompt("Give me your second number:");
// let operation = prompt("What do you whant to do with this: +, -, *, / ?");
// function result(a, b, c) {
//   let result;
//   if (c === "+") {
//     result = a + b;
//     return console.log(result);
//   } else if (c === "-") {
//     result = a - b;
//     return console.log(result);
//   } else if (c === "*") {
//     result = a * b;
//     return console.log(result);
//   } else if (c === "/") {
//     result = a / b;
//     if (result == "Infinity") {
//       return alert("Try again!");
//     } else return console.log(result);
//   }
// }

// result(numberFirst, numberSecond, operation);

//
// ДОДАТКОВЕ завдання

function getResult(a, b, c) {
  let result;
  if (c === "+") {
    result = +a + +b;
    return console.log(result);
  } else if (c === "-") {
    result = +a - +b;
    return console.log(result);
  } else if (c === "*") {
    result = +a * +b;
    return console.log(result);
  } else if (c === "/") {
    result = +a / +b;
    if (result == "Infinity") {
      return alert("Try again!");
    } else return console.log(result);
  }
}

let numberFirst = prompt("Give me your first number:");
let numberSecond = prompt("Give me your second number:");
let operation = prompt("What do you whant to do with this: +, -, *, / ?");
if (
  numberFirst !== null &&
  numberFirst !== "" &&
  numberSecond !== null &&
  numberSecond !== "" &&
  !isNaN(numberFirst) &&
  !isNaN(numberSecond)
) {
  getResult(numberFirst, numberSecond, operation);
} else
  while (true) {
    numberFirst = prompt("Give me your first number:");
    numberSecond = prompt("Give me your second number:");
    operation = prompt("What do you whant to do with this: +, -, *, / ?");
    if (
      numberFirst !== null &&
      numberFirst !== "" &&
      numberSecond !== null &&
      numberSecond !== "" &&
      !isNaN(numberFirst) &&
      !isNaN(numberSecond)
    ) {
      getResult(numberFirst, numberSecond, operation);
      break;
    }
  }
